import { defineStore } from "pinia";
//初始化时间
//多组件共享时间数据
const startDate = new Date()
const endDate = new Date()
endDate.setDate(startDate.getDate() + 1)

const useMainStore = defineStore("main", {
  state: () => ({
    token: "",

    startDate: startDate,
    endDate: endDate,
    isLoading: false
  }),
})

export default useMainStore
