/** @format */

// import { onMounted, onUnmounted, ref } from 'vue';
// import { throttle } from 'underscore'

// // console.log(throttle)

// //传入到达底部的回调函数
// // export default function useScroll(reachBottomCB) {
// //   const scrollListenerHandler = () => {
// //     const clientHeight = document.documentElement.clientHeight
// //     const scrollTop = document.documentElement.scrollTop
// //     const scrollHeight = document.documentElement.scrollHeight
// //     console.log("-------")
// //     if (clientHeight + scrollTop >= scrollHeight) {
// //       console.log("滚动到底部了")
// //       if (reachBottomCB) reachBottomCB()
// //     }
// //   }

// //   onMounted(() => {
// //     window.addEventListener("scroll", scrollListenerHandler)
// //   })

// //   onUnmounted(() => {
// //     window.removeEventListener("scroll", scrollListenerHandler)
// //   })
// // }
// // import { throttle } from 'underscore'  社区频率高的防抖节流库
// export default function useScroll(elRef) {
//   let el = window

//   const isReachBottom = ref(false)

//   const clientHeight = ref(0)
//   const scrollTop = ref(0)
//   const scrollHeight = ref(0)

//   // 防抖/节流  通过节流控制滚动事件触发频率
//   const scrollListenerHandler = throttle(() => {
//     if (el === window) {
//       clientHeight.value = document.documentElement.clientHeight
//       scrollTop.value = document.documentElement.scrollTop
//       scrollHeight.value = document.documentElement.scrollHeight
//     } else {
//       clientHeight.value = el.clientHeight
//       scrollTop.value = el.scrollTop
//       scrollHeight.value = el.scrollHeight
//     }
//     if (clientHeight.value + scrollTop.value >= scrollHeight.value) {
//       console.log("滚动到底部了")
//       isReachBottom.value = true
//     }
//   }, 100)

//   onMounted(() => {
//     if (elRef) el = elRef.value
//     el.addEventListener("scroll", scrollListenerHandler)
//   })

//   onUnmounted(() => {
//     el.removeEventListener("scroll", scrollListenerHandler)
//   })

//   return { isReachBottom, clientHeight, scrollTop, scrollHeight }
// }

import { onMounted, onUnmounted, ref } from 'vue'
import { throttle } from 'underscore'

// console.log(throttle)

// export default function useScroll(reachBottomCB) {
//   const scrollListenerHandler = () => {
//     const clientHeight = document.documentElement.clientHeight
//     const scrollTop = document.documentElement.scrollTop
//     const scrollHeight = document.documentElement.scrollHeight
//     console.log("-------")
//     if (clientHeight + scrollTop >= scrollHeight) {
//       console.log("滚动到底部了")
//       if (reachBottomCB) reachBottomCB()
//     }
//   }

//   onMounted(() => {
//     window.addEventListener("scroll", scrollListenerHandler)
//   })

//   onUnmounted(() => {
//     window.removeEventListener("scroll", scrollListenerHandler)
//   })
// }

export default function useScroll(elRef) {
	let el = window

	const isReachBottom = ref(false)

	const clientHeight = ref(0)
	const scrollTop = ref(0)
	const scrollHeight = ref(0)

	// 防抖/节流
	const scrollListenerHandler = throttle(() => {
		if (el === window) {
			clientHeight.value = document.documentElement.clientHeight
			scrollTop.value = document.documentElement.scrollTop
			scrollHeight.value = document.documentElement.scrollHeight
		} else {
			clientHeight.value = el.clientHeight
			scrollTop.value = el.scrollTop
			scrollHeight.value = el.scrollHeight
		}
		if (clientHeight.value + scrollTop.value >= scrollHeight.value) {
			console.log('滚动到底部了')
			isReachBottom.value = true
		}
	}, 100)
  
  onMounted(() => {
    if (elRef) el = elRef.value
    el.addEventListener("scroll", scrollListenerHandler)
  })
  
  onUnmounted(() => {
    el.removeEventListener("scroll", scrollListenerHandler)
  })

  return { isReachBottom, clientHeight, scrollTop, scrollHeight }
}
