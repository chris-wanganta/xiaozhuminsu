const tabbarData = [
  {
    text: "首页",
    image: "images/home.png",
    imageActive: "images/active.png",
    path: "/home"
  },
  {
    text: "收藏",
    image: "images/favor.png",
    imageActive: "images/active.png",
    path: "/favor"
  },
  {
    text: "订单",
    image: "images/order.png",
    imageActive: "images/active.png",
    path: "/order"
  },
  {
    text: "我的",
    image: "images/my.png",
    imageActive: "images/active.png",
    path: "/message"
  }
]

export default tabbarData
