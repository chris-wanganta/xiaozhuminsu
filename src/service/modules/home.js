/** @format */

import hyRequest from '../request'

// 获取首页热门推荐
export function getHomeHotSuggests() {
	return hyRequest.get({
		url: '/home/hotSuggests',
	})
}
// 获取首页分类
export function getHomeCategories() {
	return hyRequest.get({
		url: '/home/categories',
	})
}
// 获取首页房间
export function getHomeHouselist(currentPage) {
	return hyRequest.get({
		url: '/home/houselist',
		// get请求参数params  post请求参数deta
		params: {
			page: currentPage,
		},
	})
}
